#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"

#include "Game.h"
#include "Dice.h"
#include "Die.h"
#include "ScoreCard.h"
#include <typeinfo>

SCENARIO("Testing if Dice can roll 5 die objects", "[Dice]"){

	GIVEN("A Dice object") {
		Dice myTestDice;
		Die aDie;
		int numb;
		WHEN("My diceArray is filled with 5 die objects") {
			myTestDice.rollUm();
			THEN("And each Die is a number") {
				for (int i = 0; i < 5; i++){
					aDie = myTestDice.diceArray[i];
					assert(typeid(aDie) == typeid(Die));
					assert(typeid(aDie.getRoll()) == typeid(int));
				}
			}
		}
	}
}
SCENARIO("Testing ScoreCard cartagory 1 can add correctly", "[ScoreCard]"){

	GIVEN("A Score entry") {
		Dice myTestDice;
		Die aDie;
		int numb;
		ScoreCard myTestCard;
		WHEN("Rolls of atleast one number 1 die rolled"){
			aDie.setRoll(1);
			for (int i = 0; i < 5; ++i){
				myTestDice.diceArray[i] = aDie;
				aDie.setRoll(2);
			}
			THEN("Total in catagory 1 should be 1") {
				myTestCard.calcScore(myTestDice.diceArray, 1);
				assert(myTestCard.scorez[0] == 1;);
				
			}
		}
	}
}