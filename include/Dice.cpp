#include "Dice.h"
#include <iostream>
Dice::Dice(){
	for (int i = 0; i < 5; i++){
		diceArray[i] = myDie;
	}
}
Dice::~Dice(){}

void Dice::rollUm(){
	std::cout << endl;
	std::cout << "Your roll is: ";
	for (int i = 0; i < 5; i++)
	{
		myDie.roll();
		diceArray[i] = myDie;
		std::cout << diceArray[i].getRoll();
	}
	std::cout << endl << endl;
}
