#ifndef SCORE_H
#define SCORE_H
#include "Die.h"
#include "Dice.h"
#include <string>

class ScoreCard
{
public:
	ScoreCard();
	~ScoreCard();
	void Display();
	void setCatChoice(int x);
	void calcScore(Die[], int);
	int getChoice();
	int scorez[13];
	void sortDice(Die[], Die);
	int calcTotalScorez();
	int index;
	
private:
	int catChoice;
	
	int catScore;
	int gameTotal;
	int bounds;
};


#endif