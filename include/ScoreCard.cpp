#include "ScoreCard.h"
#include "Die.h"
#include "Dice.h"
#include <cmath>
#include <iostream>

ScoreCard::ScoreCard()
{
	catScore = 0;
	gameTotal = 0;
	catChoice = 0;
	for (int i = 0; i < 13; i++){
		scorez[i] = '\0';
	}
	index = 0;
}

ScoreCard::~ScoreCard(){

}
void ScoreCard::sortDice(Die Array[], Die temp){
	int i, j, flag = 1;
	for (i = 1; (i <= 5) && flag; i++){
		flag = 0;
		for (j= 0; j < 4; j++){
			if (Array[j + 1].getRoll()<Array[j].getRoll()){
				temp = Array[j];
				Array[j] = Array[j + 1];
				Array[j + 1] = temp;
				flag = 1;
			}
		}
	}

}
int ScoreCard::getChoice(){
	return catChoice;
}
void ScoreCard::calcScore(Die Array[], int catChoice){
	Die temp;
	int count = 0;
	bool condition = true;
	while (condition){
		switch (catChoice){
		case 1:
			//sum cat 1 based on dice roll
			catScore = 0;
			for (int i = 0; i < 5; i++)
			{
				if (Array[i].getRoll() == catChoice){
					catScore += 1;
				}
			}
			if (scorez[0] == '\0'){
				scorez[0] = catScore;
				condition = false;
			}
			else{
				std::cout << "A roll is already in this catagory" << std::endl;
				std::cout << "what is your new catagory choice: ";
				std::cin >> catChoice;
			}
			break;
		case 2:
			//sum cat 2 based on dice roll
			catScore = 0;
			for (int i = 0; i < 5; i++)
			{
				if (Array[i].getRoll() == catChoice){
					catScore += 2;
				}
			}
			if (scorez[1] == '\0'){
				scorez[1] = catScore;
				condition = false;
			}
			else{
				std::cout << "A roll is already in this catagory" << std::endl;
				std::cout << "what is your new catagory choice: ";
				std::cin >> catChoice;
			}

			break;
		case 3:
			catScore = 0;
			for (int i = 0; i < 5; i++)
			{
				if (Array[i].getRoll() == catChoice){
					catScore += 3;
				}
			}
			if (scorez[2] == '\0'){
				scorez[2] = catScore;
				condition = false;
			}
			else{
				std::cout << "A roll is already in this catagory" << endl;
				std::cout << "what is your new catagory choice: ";
				std::cin >> catChoice;
			}
			//sum cat 3 based on dice roll
			break;
		case 4:
			//sum cat 4 based on dice roll
			catScore = 0;
			for (int i = 0; i < 5; i++)
			{
				if (Array[i].getRoll() == catChoice){
					catScore += 4;
				}
			}
			if (scorez[3] == '\0'){
				scorez[3] = catScore;
				condition = false;
			}
			else{
				std::cout << "A roll is already in this catagory" << endl;
				std::cout << "what is your new catagory choice: ";
				std::cin >> catChoice;
			}
			break;
		case 5:
			//sum cat 5 based on dice roll
			catScore = 0;
			for (int i = 0; i < 5; i++)
			{
				if (Array[i].getRoll() == catChoice){
					catScore += 5;
				}
			}
			if (scorez[4] == '\0'){
				scorez[4] = catScore;
				condition = false;
			}
			else{
				std::cout << "A roll is already in this catagory" << endl;
				std::cout << "what is your new catagory choice: ";
				std::cin >> catChoice;
			}
			break;
		case 6:
			//sum cat 6 based on dice roll
			catScore = 0;
			for (int i = 0; i < 5; i++)
			{
				if (Array[i].getRoll() == catChoice){
					catScore += 6;
				}
			}
			if (scorez[5] == '\0'){
				scorez[5] = catScore;
				condition = false;
			}
			else{
				std::cout << "A roll is already in this catagory" << endl;
				std::cout << "what is your new catagory choice: ";
				std::cin >> catChoice;
			}
			break;
		case 7:
			//sum cat 7 three of a kind based on dice roll
			sortDice(Array, temp);
			catScore = 0;
			index = 0;
			if (Array[index].getRoll() == Array[index + 1].getRoll() && Array[index + 1].getRoll() == Array[index + 2].getRoll()
				|| Array[index + 2].getRoll() == Array[index + 3].getRoll() && Array[index + 3].getRoll() == Array[index + 4].getRoll()){
				for (int j = 0; j < 5; j++){
					catScore += Array[j].getRoll();
				}
			}
			if (scorez[6] == '\0'){
				scorez[6] = catScore;
				condition = false;
				
			}
			else{
				std::cout << "A roll is already in this catagory" << endl;
				std::cout << "what is your new catagory choice: ";
				std::cin >> catChoice;
			}
			break;
		case 8:
			//sum cat 8 four of a kind based on dice roll
			sortDice(Array, temp);
			catScore = 0;
			index = 0;
			if (Array[index].getRoll() == Array[index + 1].getRoll() && Array[index + 1].getRoll() == Array[index + 2].getRoll() && Array[index + 2].getRoll() && Array[index + 3].getRoll()
				|| Array[index + 1].getRoll() == Array[index + 2].getRoll() && Array[index + 2].getRoll() == Array[index + 3].getRoll() && Array[index + 3].getRoll() == Array[index + 4].getRoll()){
				for (int j = 0; j < 5; j++){
					catScore += Array[j].getRoll();
				}
			}
			if (scorez[8] == '\0'){
				scorez[8] = catScore;
				condition = false;
			}
			else{
				std::cout << "A roll is already in this catagory" << endl;
				std::cout << "what is your new catagory choice: ";
				std::cin >> catChoice;
			}
			break;
		case 9:
			//sum cat 9 Full house based on dice roll
			sortDice(Array, temp);
			catScore = 0;
			index = 0;
			if (Array[index].getRoll() == Array[index + 1].getRoll() && Array[index + 2].getRoll() == Array[index + 3].getRoll() && Array[index + 3].getRoll() == Array[index + 4].getRoll()
				|| Array[index].getRoll() == Array[index + 1].getRoll() && Array[index + 1].getRoll() == Array[index + 2].getRoll() && Array[index + 3].getRoll() == Array[index + 4].getRoll()){
				catScore = 25;
			}
			if (scorez[8] == '\0'){
				scorez[8] = catScore;
				condition = false;
			}
			else{
				std::cout << "A roll is already in this catagory" << endl;
				std::cout << "what is your new catagory choice: ";
				std::cin >> catChoice;
			}

			break;
		case 10:
			//sum cat 10 Small straight based on dice roll
			sortDice(Array, temp);
			catScore = 0;
			index = 0;
			if (abs((Array[index].getRoll() - Array[index + 1].getRoll())) == 1 && abs((Array[index + 1].getRoll() - Array[index + 2].getRoll())) == 1
				&& abs((Array[index + 2].getRoll() - Array[index + 3].getRoll())) == 1){
				catScore = 30;
			}
			if (scorez[9] == '\0'){
				scorez[9] = catScore;
				condition = false;
			}
			else{
				std::cout << "A roll is already in this catagory" << endl;
				std::cout << "what is your new catagory choice: ";
				std::cin >> catChoice;
			}
			break;
		case 11:
			//sum cat 11 Large straight based on dice roll
			sortDice(Array, temp);
			catScore = 0;
			index = 0;
			if (abs((Array[index].getRoll() - Array[index + 1].getRoll())) == 1 && abs((Array[index + 1].getRoll() - Array[index + 2].getRoll())) == 1
				&& abs((Array[index + 2].getRoll() - Array[index + 3].getRoll())) == 1 && abs((Array[index + 3].getRoll() - Array[index + 4].getRoll())) == 1){
				catScore = 40;
			}
			if (scorez[10] == '\0'){
				scorez[10] = catScore;
				condition = false;
			}
			else{
				std::cout << "A roll is already in this catagory" << endl;
				std::cout << "what is your new catagory choice: ";
				std::cin >> catChoice;
			}
			break;
		case 12:
			//sum cat 12 Yahtzee based on dice roll
			sortDice(Array, temp);
			catScore = 0;
			for (int j = 0; j < 5; j++){
				if (Array[j].getRoll() == Array[j + 1].getRoll()){
					count++;
				}
			}
			if (scorez[11] == '\0' && count == 5){
				catScore = 50;
				scorez[11] = catScore;
				condition = false;
				std::cout << "you got a YAHTZEE!! its the only one you can store because of limitations";
			}
			else{
				std::cout << "A roll is already in this catagory" << endl;
				std::cout << "what is your new catagory choice: ";
				std::cin >> catChoice;
			}

			break;
		case 13:
			//sum cat 13 Chance based on dice roll
			catScore = 0;
			for (int i = 0; i < 5; i++)
			{
				catScore += Array[i].getRoll();
			}
			if (scorez[12] == '\0'){
				scorez[12] = catScore;
				condition = false;
			}
			break;

		default:
			std::cout << "invalid choice number" << endl;
			std::cout << "what is your new catagory choice: ";
			std::cin >> catChoice;
		break;
		}
	}
	
}
int ScoreCard::calcTotalScorez(){
	for(int i = 0; i < 13; i++){
		gameTotal += scorez[i];
	}
	return gameTotal;
}
void ScoreCard::Display(){
	std::cout << "Upper Section\nAces: 1\nTwos: 2\nThrees: 3\nFours: 4\nFives: 5\nSixes: 6\n\nLower Section\nThree of a kind: 7\nFour of a kind: 8\nFull House: 9\nSmall Straight: 10\nLarge Straight: 11\nYahtZee: 12\nChance: 13\n";
	std::cout << "What catagory would you like this roll to fill ";
	std::cin >> catChoice;
}